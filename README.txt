
Description
-------------
Role Login enables login to be restricted by role. The module was developed to
enable a user table to be shared between multiple instances, but still maintain 
control over which users can login, for example, on a dev instance versus a 
live site. 


Dependencies
-------------
None


Installation
-------------
1. install module: copy role_login directory and all its contents to your 
   modules directory
2. enable module: admin/build/modules
3. configure module: admin/user/roles/role_login


Usage
-------------
After enabling the module, configure one or more roles that will be able to 
login on the site. If no roles are selected, then the role login check will be 
bypassed. The superuser (account 1) can never be locked out regardless of Role 
Login setting.


Download
-------------
Download package and report bugs, feature requests, or submit a patch from the 
project page on the Drupal web site.
http://drupal.org/project/role_login


Todo List
-------------
None


Author
-------------
John Money
ossemble LLC.
http://ossemble.com

Module development sponsored by ConsumerSearch, a service of About.com, a part
of The New York Times Company.
http://www.consumersearch.com
